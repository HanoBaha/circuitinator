#ifndef __PLATYPUS__
#define __PLATYPUS__

#include <string>
#include <iostream>

/*
You are like Dr. Doofenschmirtz, trying to spread fear and tears among mere mortals with your bad code.
But be warned, a platypus is roaming your code.
And before you even notice, Perry the Platypus will reveal itself and defeat your devilish plan.
*/


class Platypus {
public:
    void testing(const std::string& function);
    void error(std::string desc);

private:
    std::string m_cur_func;
};

std::string human_readable(const uint8_t& byte);

#endif