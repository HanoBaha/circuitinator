#include "platypus.hpp"

void Platypus::testing(const std::string& function) {
    m_cur_func = std::move(function);
    std::cout << "Testing " << m_cur_func << " ...\n";
}

void Platypus::error(std::string desc) {
    std::cout << "  error : " << desc << "\n";
}

std::string human_readable(const uint8_t& byte) {
    std::string ret;
    unsigned char mask = 0b10000000;

    for(int i = 0; i < 8; i++) {
        ret += '0' + ((byte & mask) >> (7 - i));
        mask >>= 1;
    }

    return ret;
}