#include "component.hpp"

class Simulator {
public:
    Simulator(Component<0, 0>* circuit) : m_circuit(circuit), step_count(0) {};

    void step() 
    {
        std::cout << step_count++ << " -----\n";
        WireBase::propagate_all();
        m_circuit->simulate();
    }

private:
    Component<0, 0>* m_circuit;
    int step_count;
};