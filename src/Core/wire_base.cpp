#include "wire_base.hpp"

#include "../Utils/platypus.hpp"
#include "wire.hpp"

std::forward_list<WireBase*> WireBase::s_wires;

WireBase::WireBase() 
{
	s_wires.push_front(this);
}

WireBase::~WireBase()
{
	s_wires.remove(this);
}

void WireBase::propagate_all()
{
    for(WireBase*& w : WireBase::s_wires)
        w->propagate();
}

void WireBase::test()
{
    Platypus perry;

    perry.testing("propagate_all()");
    
    Wire<4> a;
    Wire<8> b;
    Wire<16> c;

    Bit* aa = new Bit[16];
    for(int i = 0; i < 16; i++)
        aa[i] = Bit::on;

    a.write(0, 4, aa);
    b.write(0, 8, aa);
    c.write(0, 16, aa);

    WireBase::propagate_all();

    aa = (Bit*)a.read(0, 4);
    for(int i = 0; i < 4; i++)
        if(aa[i] != Bit::on)
            perry.error("propagation didn't work on a");

    aa = (Bit*)b.read(0, 8);
    for(int i = 0; i < 8; i++)
        if(aa[i] != Bit::on)
            perry.error("propagation didn't work on a");

    aa = (Bit*)c.read(0, 16);
    for(int i = 0; i < 16; i++)
        if(aa[i] != Bit::on)
            perry.error("propagation didn't work on a");
}