#ifndef __COMPONENT_BASE__
#define __COMPONENT_BASE__

#include "bit.hpp"

class ComponentBase {
public:
    virtual void definition() = 0;
    virtual void behaviour() = 0;
    virtual void simulate() = 0;

    virtual const Bit* read(std::size_t bit_start_pos, int count) = 0;
    virtual void write(std::size_t bit_start_pos, int count, const Bit* bits) = 0;
private:
};

#endif