#pragma once
#include "../Utils/platypus.hpp"
#include "wire.hpp"
#include "wire_base.hpp"

#include <cassert>

#define tparam int bits_count
#define targs bits_count

template<tparam>
Wire<targs>::Wire() : WireBase() 
{
    for(std::size_t i = 0; i < bits_count; i++)
        m_incoming_data[i] = m_current_data[i] = Bit::undefined;
}

template<tparam>
const Bit* Wire<targs>::read(std::size_t bit_start_pos, int count) const
{
    if(bit_start_pos + count <= bits_count)
        return &m_current_data[bit_start_pos];
    return nullptr;
}

template<tparam>
void Wire<targs>::write(std::size_t bit_start_pos, int count, const Bit* bits)
{
    if(bit_start_pos + count <= bits_count)
            memcpy(m_incoming_data, bits, count * sizeof(Bit));
}

template<tparam>
void Wire<targs>::propagate()
{
    memcpy(m_current_data, m_incoming_data, bits_count * sizeof(Bit));
    for(std::size_t i = 0; i < bits_count; i++)
        m_incoming_data[i] = Bit::undefined;
}

template<tparam>
void Wire<targs>::test() 
{
    Platypus perry; // He's a detective, and he helps me find bugs
    Wire<8> wire;

    perry.testing("read() bit vector");
    {
        for(int i = 0; i < 8; i++)
            wire.m_current_data[i] = Bit::on;
        
        const Bit* res = wire.read(0, 8);
        if(res)
            for(int i = 0; i < 8; i++)
            {
                if(res[i] != Bit::on)
                    perry.error("read " + std::to_string(int(res[i])) + " rather than 2 on bit " + std::to_string(i));
            }
        else
            perry.error("invalid read size");
    }
    perry.testing("write() bit vector");
    {
        for(int i = 0; i < 8; i++)
            wire.m_incoming_data[i] = Bit::undefined;

        Bit input[8] = {Bit::off, Bit::on, Bit::error, Bit::off, Bit::on, Bit::error, Bit::off, Bit::on};
        wire.write(0, 8, input);

        for(int i = 0; i < 8; i++)
        {
            if(wire.m_incoming_data[i] != input[i])
                perry.error("wrote " + std::to_string(int(wire.m_incoming_data[i])) 
                   + " rather than " + std::to_string(int(input[i])) 
                        + " on bit " + std::to_string(i));
        }
    }
    
    perry.testing("propagate()");
    {
        Bit input[8] = {Bit::off, Bit::on, Bit::off, Bit::on, Bit::off, Bit::on, Bit::off, Bit::on};
        for(int i = 0; i < 8; i++)
            wire.m_incoming_data[i] = input[i];
        
        wire.propagate();

        for(int i = 0; i < 8; i++)
        {
            if(wire.m_current_data[i] != input[i])
                perry.error("propagate " + std::to_string(int(wire.m_current_data[i])) 
                       + " rather than " + std::to_string(int(input[i]))
                            + " on bit " + std::to_string(i));
            if(wire.m_incoming_data[i] != Bit::undefined)
                perry.error("bit " + std::to_string(i) + " wasn't reset");
        }
    }
    
}

#undef tparam
#undef targs