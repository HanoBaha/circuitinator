// On passe les paramètres en template
//      car l'état initial sera le même pour les mêmes paramètres
// On calcul les paramètres / état du composant (si possible) at compile time
// Un composant a une forme classique registre -> Calculation -> Sélection
//      où chaque tick permet d'itérer et modifier l'état interne.
// Niveau implémentation, on pourrait avoir une méthode qui demande l'output récursivement:
//      Notre accumulateur final (pour avoir le résultat) demande a son input un chiffre
//      L'input est un ptr "connecté" à l'output d'un composant
//      Ce composant va donc réaliser le calcul, mais pour ça il va d'abord demander un chiffre à ses entrées
//      etc. jusqu'à arriver aux registres initiaux qui contiennent les nombres sur lesquels ont veut opérer
// Niveau implémentation, on doit pouvoir instancier des opérateurs, et récupérer leur input/output spécifiquement 
//      pour les passer en paramètre d'une fonction "lier" ou qqlch comme ça.
//      Possiblement détecter les boucles et les interdire ou des trucs comme ça faut voir

#ifndef __COMPONENT__
#define __COMPONENT__

#include "component_base.hpp"
#include "wire.hpp"

#include <unordered_map>
#include <string>

#define tparam int input_size, int output_size
#define targs input_size, output_size

#define INPUT this->input
#define OUTPUT this->output

template<tparam>
class Component : public ComponentBase {
public:

    void simulate() final;

    const Bit* read(std::size_t bit_start_pos, int count) final;
    void write(std::size_t bit_start_pos, int count, const Bit* bits) final;

protected:
    std::unordered_map<std::string, WireBase*> wires;
    std::unordered_map<std::string, ComponentBase*> components;

    Wire<input_size> input;
    Wire<output_size> output;
};


#undef tparam
#undef targs


#include "component.tpp"

#endif