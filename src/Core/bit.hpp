#ifndef __BIT__
#define __BIT__

#include <cstdint>

enum class Bit : uint8_t {
    undefined = 0, off = 1, on = 2, error = 3
};

#endif