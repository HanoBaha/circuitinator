#ifndef __WIRE_BASE__
#define __WIRE_BASE__

#include "bit.hpp"

#include <forward_list>

class WireBase {
public:
    /// @brief Constructor will push created instance to s_wires. 
    /// It HAS to be called by child constructor
    WireBase();

    ~WireBase();


    virtual const Bit* read(std::size_t bit_start_pos, int count) const = 0;

    virtual void write(std::size_t bit_start_pos, int count, const Bit* bits) = 0;

    /// @brief call propagate() for each wire templated class
    static void propagate_all();
    
    /// @brief function which propagate signal through each wire
    virtual void propagate() = 0;

    static void test();

private:
    /// @brief keep track of each instances of wires
    static std::forward_list<WireBase*> s_wires;
};

#endif