#pragma once
#include "component.hpp"
#include "component_base.hpp"

#define tparam int input_size, int output_size
#define targs input_size, output_size

template<tparam>
void Component<targs>::simulate() 
{
    input.propagate();
    behaviour();
    for(const auto& pair : components)
    {
        pair.second->simulate();
    }
}

template<tparam>
const Bit* Component<targs>::read(std::size_t bit_start_pos, int count)
{
    return output.read(bit_start_pos, count);   
}

template<tparam>
void Component<targs>::write(std::size_t bit_start_pos, int count, const Bit* bits)
{
    input.write(bit_start_pos, count, bits);
}

#undef tparam
#undef targs