#ifndef __WIRE__
#define __WIRE__

#include "wire_base.hpp"

#include <cstring>
#include <iostream>
#include <string>
#include <vector>

// Basically bus of given size
template<int bits_count>
class Wire : public WireBase {
public:
    Wire();

    /// @brief return a pointer to continuous bits array (bit_start_pos is the LSB).
    /// Return nullptr if the given count exceed array capacity.
    /// bits are numbered left to right, as their corresponding power of 2
    const Bit* read(std::size_t bit_start_pos, int count) const override;

    /// @brief write the given bits to the given position (bits[0] should be the LSB)
    /// It won't copy if the given count exceed array capacity.
    /// bits are numbered left to right, as their corresponding power of 2
    void write(std::size_t bit_start_pos, int count, const Bit* bits) override;

    /// @brief implementation of WireBase::propagate() virutal function
    /// propagate the signal from incoming to current data
    void propagate() override;

    static void test();

private:
    // We store 1 virtual bit onto 2 real bits. Because the smaller allocation
    // we can make is 1 byte, we're allocating 4 virutal bits at a time.
    Bit m_current_data[bits_count];
    // The simulation will be run on the same model as the Game of Life:
    // Update is done on a "grid" which isn't the working grid
    // And when everything is updated, we copy the updated grid to the working grid.
    Bit m_incoming_data[bits_count];
};

#include "wire.tpp"

#endif