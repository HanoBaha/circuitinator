#include "Core/component.hpp"
#include "Core/simulator.hpp"
#include "Components/register.hpp"
#include "Components/display.hpp"

class Circuit : public Component<0, 0> {
public:
    Circuit() {
        definition();
    }

    void definition() final
    {
        components["reg8_a"] = new Register<8>;
        components["reg8_b"] = new Register<8>;
        components["dIn"] = new Display<8>("input");
        components["dMid"] = new Display<8>("reg8_a");
        components["dOut"] = new Display<8>("reg8_b");

        wires["init"] = new Wire<8>;
        const Bit value[8] = {Bit(0), Bit(1), Bit(2), Bit(3), Bit(2), Bit(1), Bit(0), Bit(1)};
        wires["init"]->write(0, 8, value);
    }

    void behaviour() final 
    {
        // display init
        components["dIn"]->write(0, 8, wires["init"]->read(0, 8));

        // put init in reg8_a
        components["reg8_a"]->write(0, 8, wires["init"]->read(0, 8));

        // display reg8_a
        components["dMid"]->write(0, 8, components["reg8_a"]->read(0, 8));

        // put reg8_a in reg8_b
        components["reg8_b"]->write(0, 8, components["reg8_a"]->read(0, 8));

        // display reg8_b
        components["dOut"]->write(0, 8, components["reg8_b"]->read(0, 8));

        // loop the signal
        wires["init"]->write(0, 8, components["reg8_b"]->read(0, 8));
    }
};

int main(void)
{
    Circuit c;

    Simulator s(&c);

    s.step();
    s.step();
    s.step();
    s.step();
    s.step();
    s.step();
    s.step();
    s.step();
    s.step();
    s.step();
    s.step();

    return 0;
}