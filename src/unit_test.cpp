#include "redundant_fixed_point.hpp"
#include "Core/wire.hpp"
#include "Core/wire_base.hpp"

#include <time.h>

int main(void)
{
    unsigned int seed = time(NULL);
    std::cout << "Starting test with seed: " << seed << "\n"; 
    srand(seed);

    //rFixedPoint<10, 5, 10>::test();
    Wire<8>::test();
    WireBase::test();
    
    return 0;
}