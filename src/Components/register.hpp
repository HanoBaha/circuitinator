#ifndef __REGISTER__
#define __REGISTER__

#include "../Core/component.hpp"

#define targs_comp size, size


template<int size>
class Register : public Component<size, size> {
public:
    void definition() final {}

    void behaviour() final
    {
        OUTPUT.write(0, size, INPUT.read(0, size));
    }   

};


#undef targs_comp

#endif