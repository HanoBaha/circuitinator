#include "../Core/component.hpp"

#include <iostream>

#define targs_comp size, 0


template<int size>
class Display : public Component<targs_comp> {
public:
    Display(const char* name)  :
        m_name(name)
    {}
    
    void definition() final {};

    void behaviour() final  {
        const Bit* res = INPUT.read(0, size);

        if(!res)
            return;

        std::cout << m_name << " ";
        for(int i = 0; i < size; i++)
        {
            if(i % 8 == 0)
                std::cout << " ";
            std::cout << int(res[i]);
        }
        std::cout << std::endl;
    }

private:
    const char* m_name;
};


#undef targs_comp