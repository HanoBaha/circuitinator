#ifndef __SIGNED_NUMBER__
#define __SIGNED_NUMBER__

#include <array>
#include <iostream>
#include <math.h>
#include <utility>

using digit = char;
#define tparam int radix, int max_digit, int precision
#define targs radix, max_digit, precision

// radix : number's base
// max_digit   : upper bound of the digit representation {-max_digit, ..., 0, ..., max_digit}
// m     : precision, as in number of digit cointained in the float
template <tparam>
class rFixedPoint {
public:
    rFixedPoint();
    rFixedPoint(const std::string& n);
    rFixedPoint(const rFixedPoint<targs>& rf);
    rFixedPoint(rFixedPoint<targs>&& rf);

    // for debug purposes
    rFixedPoint(const int& dc, const int& po, const bool& ps, const std::array<digit, precision>& a) :
        m_digit_count(dc), m_integer_digits(po), m_point_set(ps)
    {
        for(int i = 0; i < precision ; i++)
            m_digits[i] = a[i];
    }

    ~rFixedPoint() = default;

    rFixedPoint<targs>& operator=(const rFixedPoint<targs>& rf);
    rFixedPoint<targs>& operator=(rFixedPoint<targs>&& rf);
    // Parse the string to construct the number
    rFixedPoint<targs>& operator=(const std::string& n);

    digit& operator[](std::size_t offset) { return m_digits[offset]; }
    const digit& operator[](std::size_t offset) const { return m_digits[offset]; }

    rFixedPoint<targs> operator+(const rFixedPoint<targs>& rf) const;
    rFixedPoint<targs>& operator+=(const rFixedPoint<targs>& rf);
    rFixedPoint<targs> operator-() const;
    rFixedPoint<targs> operator-(const rFixedPoint<targs>& rf) const;
    rFixedPoint<targs>& operator-=(const rFixedPoint<targs>& rf);
    rFixedPoint<targs> operator*(const rFixedPoint<targs>& rf) const;

    // Append a digit to the number
    rFixedPoint<targs>& append(digit d);
    rFixedPoint<targs>& operator<<(char d) { return append(d); }
    rFixedPoint<targs>& operator<<(int d) { return append(d); }

    // Return actual value of the number
    operator float() const;

    // TODO : ne pas afficher les 0 précédent et antécédent
    friend std::ostream& operator<< (std::ostream& stream, const rFixedPoint<targs>& rf) {
        int total_digit = precision;

        // get rid of trailing 0
        while(total_digit > 0 && rf[--total_digit] == 0);

        for(int i = 0; i <= total_digit; i++) {
            if(i == rf.m_integer_digits)
                stream << ".";
            if(rf[i] < 0)
                stream << "\x1b[53m" << std::hex << -(int)rf[i] << std::dec << "\x1b[55m"; // Overline on top of negative digit
            else
                stream << std::hex << (int)rf[i] << std::dec;
        }
        return stream;
    }

    static void test();

private:
    int m_digit_count;
    int m_integer_digits; // point position in the number
    bool m_point_set; // true if user specified point position
    std::array<digit, precision> m_digits;
};


//===============================
//        construction
//===============================

template<tparam>
rFixedPoint<targs>::rFixedPoint() : m_digit_count(0), m_integer_digits(0), m_point_set(false) {}

template<tparam>
rFixedPoint<targs>::rFixedPoint(const std::string& n) 
    : m_digit_count(0), m_integer_digits(0), m_point_set(false)
{
    (*this) = n;
}

template<tparam>
rFixedPoint<targs>::rFixedPoint(const rFixedPoint<targs>& rf) : 
    m_digit_count(rf.m_digit_count), m_integer_digits(rf.m_integer_digits), m_point_set(rf.m_point_set)
{
    for(int i = 0; i < precision; i++)
        m_digits[i] = rf[i];
}

template<tparam>
rFixedPoint<targs>::rFixedPoint(rFixedPoint<targs>&& rf) :
    m_digit_count(std::exchange(rf.m_digit_count, 0)),
    m_integer_digits(std::exchange(rf.m_integer_digits, 0)),
    m_point_set(std::exchange(rf.m_point_set, false)),
    m_digits(std::move(rf.m_digits))
{}

template<tparam>
rFixedPoint<targs>& rFixedPoint<targs>::operator=(const rFixedPoint<targs>& rf) {
    m_digit_count = rf.m_digit_count;
    m_integer_digits = rf.m_integer_digits;
    m_point_set = rf.m_point_set;
    for(int i = 0; i < precision; i++)
        m_digits[i] = rf[i];

    return *this;
}

template<tparam>
rFixedPoint<targs>& rFixedPoint<targs>::operator=(rFixedPoint<targs>&& rf) 
{
    if(&rf != this) {
        m_digit_count = std::exchange(rf.m_digit_count, 0);
        m_integer_digits = std::exchange(rf.m_integer_digits, 0);
        m_point_set = std::exchange(rf.m_point_set, false);
        m_digits = std::move(rf.m_digits);
    }
    return *this;
}


template <tparam>
rFixedPoint<targs>& rFixedPoint<targs>::append(digit d) {
    // offset the point until user add it or we reach end of precision
    if(d != ',' && d != '.') {
        if(m_digit_count < precision)
            m_digits[m_digit_count++] = d;
        if(!m_point_set)
            m_integer_digits++;            
    }
    else
        m_point_set = true;

    return *this;
}

template <tparam>
rFixedPoint<targs>& rFixedPoint<targs>::operator=(const std::string& n)
{
    bool was_neg = false;
    char digit;

    for(const char& d : n) {
        int offset = '0';
        if(d == '.' || d == ',')
            digit = d;
        else {
            if(d >= 'a' && d <= 'f')
                offset = 87;
            digit = (was_neg ? -1 : 1) * (d - offset);
        }
        if((was_neg = (d == '-')))
            continue;
        append(digit);
    }

    m_point_set = true;
    for(int i = n.length() - 1; i < precision; i++)
        append(0);
    return *this;
}


//===============================
//     arithmetic operations
//===============================

// Align numbers relative to their point position
// A and B are inputs and output
// return max offset
template<int precision>
int static inline align_point(std::array<digit, precision>& A, int A_offset, std::array<digit, precision>& B, int B_offset)
{
    std::array<digit, precision>* arr_ptr;
    int diff_offset = A_offset - B_offset;

    arr_ptr = (diff_offset < 0 ? &A : &B);
    diff_offset = std::abs(diff_offset);

    // Align digit in b
    for(int i = precision - 1; i >= 0; i--)
        (*arr_ptr)[i] = (i >= diff_offset ? (*arr_ptr)[i - diff_offset] : 0);

    return std::max(A_offset, B_offset);
}

// Addition from 
// Digital Arithmetic - M.D. Ercegovac & T. Lang
// Expression 9.16
template <tparam>
rFixedPoint<targs> rFixedPoint<targs>::operator+(const rFixedPoint<targs>& rf) const 
{
    rFixedPoint<targs> res;
    
    std::array<digit, precision> A = m_digits,
                         B = rf.m_digits;

    int max_offset = align_point<precision>(A, m_integer_digits, B, rf.m_integer_digits);

    // Do the actual sum computation
    int t = 0, t_1, w_1, tmp_sum;
    for(int i = precision - 1; i >= 0; i--) {
        tmp_sum = A[i] + B[i];
        if(tmp_sum >= max_digit) 
            { t_1 =  1; w_1 = tmp_sum - radix; }
        else if(tmp_sum <= -max_digit)
            { t_1 = -1; w_1 = tmp_sum + radix; }
        else
            { t_1 =  0; w_1 = tmp_sum;         }

        res[i] = w_1 + t;
        t = t_1;
    }
    res.m_integer_digits = max_offset;
    return res;
}

template<tparam>
rFixedPoint<targs>& rFixedPoint<targs>::operator+=(const rFixedPoint<targs>& rf) 
{
    *this = *this + rf;
    return *this;
}

template<tparam>
rFixedPoint<targs>& rFixedPoint<targs>::operator-=(const rFixedPoint<targs>& rf) 
{
    *this = *this - rf;
    return *this;
}

// TODO : fix multiplication
template<tparam>
rFixedPoint<targs> rFixedPoint<targs>::operator*(const rFixedPoint<targs>& rf) const
{
    rFixedPoint<targs> Z("0");
    
    // We'll do the multiplication left to right, and all digits coming after position m will be lost.
    int partial_mult, zi;
    int carry_offset = ((m_digits[0] * rf.m_digits[0]) / radix != 0); // if the first multiplication produce a carry 
        //                                            we have to offset the position were we're inserting resulting digit

    // Do the actual computation, like schoolbook, but left to right
    for(int bi = 0; bi < precision; bi++) {
        for(int ai = 0; ai < precision - bi - carry_offset; ai++) {
        //                      ^^^^^^^^^^^^^^^^^  ensure that we're not doing calculation which won't be save to result
            partial_mult = m_digits[ai] * rf.m_digits[bi];

            zi = ai + bi + carry_offset;

            Z[zi] += partial_mult % radix;
            Z[zi - 1] += partial_mult / radix;
        }
    }

    // Keep digit representation inside the redundant bound (max_digit)
    for(zi = precision - 1; zi >= 1; zi--) {
        Z[zi - 1] += Z[zi] / radix;
        Z[zi] = Z[zi] % radix;
        if(Z[zi] > max_digit) {
            Z[zi] = Z[zi] - radix;
            Z[zi - 1] += 1;
        }
    }

    
    Z.m_integer_digits = m_integer_digits + rf.m_integer_digits - 1 + carry_offset;

    return Z;
}

template <tparam>
rFixedPoint<targs> rFixedPoint<targs>::operator-() const {
    rFixedPoint<targs> res = *this;
    for(int i = 0; i < precision; i++)
        res[i] = -res[i];
    return res;
}

template <tparam>
rFixedPoint<targs> rFixedPoint<targs>::operator-(const rFixedPoint<targs>& rf) const {
    return *this + -rf;
}

//===============================
//         conversion
//===============================

template <tparam>
rFixedPoint<targs>::operator float() const {
    double res = 0.0;
    double offset_value = std::pow(radix, m_integer_digits - 1);

    // x = sum 0->precision (xi * r ^ -i)
    for(int i = 0; i < precision; i++)
    {
        res += m_digits[i] * offset_value;
        offset_value /= radix;
    }

    return res; 
}

//===============================
//             test
//===============================

template <tparam>
static inline bool test_mult()
{
    int nb_error = 0;
    double error_tolerance = radix * 0.5 * std::pow(radix, -(precision * 0.75));

    // Assume that conversion from rFixingPoint to float is working
    rFixedPoint<targs> A, B, Z;
    float a, b, z;
    for(int i = 0; i < 100; i++)
    {
        // Init 2 number with digit between [-max_digit; max_digit]
        for(int j = 0; j < precision; j++) {
            A[j] = (rand() % (2 * max_digit + 1)) - max_digit; 
            B[j] = (rand() % (2 * max_digit + 1)) - max_digit;
        }

        Z = A * B;
        a = (float)A;
        b = (float)B;
        z = a * b;

        std::cout.precision(precision);
        if(std::abs(Z - z) > error_tolerance) {
            std::cout << A << " * " << B << " found " << Z << " (" << (float)Z <<") where\n"
                      << a << " * " << b << "   =   " << z << "\n";
            
        }
        std::cout.precision(6);
    }

    return !nb_error;
}

template<int precision>
static inline bool test_align_point()
{
    int nb_error = 0;
    std::array<digit, precision> A, B, A_raw, B_raw;

    // Init
    for(int i = 0; i < precision; i++) {
        A_raw[i] = i;
        B_raw[i] = -i;
    }

    // Test for all possible offset differencies
    for(int diff_offset = 0; diff_offset < precision ; diff_offset++) 
    {
        // One way
        A = A_raw;
        B = B_raw;

        align_point<precision>(A, 0, B, diff_offset);
        for(int i = 0; i < precision; i++) {
            if(i < diff_offset) {
                if(A[i] != 0) nb_error++;
            } else
                if(A[i] != A_raw[i - diff_offset]) nb_error++;
            if(B[i] != B_raw[i]) nb_error++;
        }

        // The other way
        A = A_raw;
        B = B_raw;
        
        align_point<precision>(B, 0, A, diff_offset);
        for(int i = 0; i < precision; i++) {
            if(i < diff_offset) {
                if(B[i] != 0) nb_error++;
            } else
                if(B[i] != B_raw[i - diff_offset]) nb_error++;
            if(A[i] != A_raw[i]) nb_error++;
        }
    }


    return !nb_error;
}

template <tparam>
void rFixedPoint<targs>::test() 
{
    std::cout << "Testing rFixedPoint.\n\n";

    // rFixedPoint();
    // rFixedPoint(const std::string& n);
    // rFixedPoint(const rFixedPoint<targs>& rf);
    // rFixedPoint(rFixedPoint<targs>&& rf);
    // rFixedPoint(const int& dc, const int& po, const bool& ps, const std::array<digit, m>& a);
    // rFixedPoint<targs>& operator=(const rFixedPoint<targs>& rf);
    // rFixedPoint<targs>& operator=(rFixedPoint<targs>&& rf);
    // rFixedPoint<targs>& operator=(const std::string& n);
    // digit& operator[](std::size_t offset) { return m_digits[offset]; }
    // const digit& operator[](std::size_t offset) const { return m_digits[offset]; }

    // rFixedPoint<targs> operator+(const rFixedPoint<targs>& rf) const;
    // rFixedPoint<targs>& operator+=(const rFixedPoint<targs>& rf);
    // rFixedPoint<targs> operator-() const;
    // rFixedPoint<targs> operator-(const rFixedPoint<targs>& rf) const;
    // rFixedPoint<targs>& operator-=(const rFixedPoint<targs>& rf);
    // rFixedPoint<targs> operator*(const rFixedPoint<targs>& rf) const;
    if(!test_mult<targs>())
        std::cout << "Multiplication failed\n";
    
    // rFixedPoint<targs>& append(digit d);
    // rFixedPoint<targs>& operator<<(char d) { return append(d); }
    // rFixedPoint<targs>& operator<<(int d) { return append(d); }
    // operator float() const;

    // int align_point(std::array<digit, m>& A, int A_offset, std::array<digit, m>& B, int B_offset);
    if(!test_align_point<precision>())
        std::cout << "Align point failed.\n";
}

#undef tparam
#undef targs

#endif